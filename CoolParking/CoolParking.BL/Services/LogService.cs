﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string logFilePath)
        {
            if (logFilePath == "")
                throw new ArgumentException();
            else
                LogPath = logFilePath;
        }

        public string LogPath { get; }

        public string Read()
        {
            string fileContent;
            if (!File.Exists(LogPath))
            {
                throw new FileNotFoundException();
            }
            using (StreamReader streamReader = new StreamReader(LogPath))
            {
                fileContent = streamReader.ReadToEnd();
                streamReader.Close();
            }
            return fileContent;
        }

        public void Write(string logInfo)
        {
            using (StreamWriter streamWriter = new StreamWriter(LogPath, append: true))
            {
                streamWriter.WriteLine(logInfo);
                streamWriter.Close();
            }
        }
    }

}