﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        ITimerService _withdrawTimer;
        ITimerService _logTimer;
        ILogService _logService;
        List<TransactionInfo> transactionsList;

        Parking parking = Parking.GetInstance();        

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += Withdrawing;
            _logTimer = logTimer;
            _logService = logService;
            _logTimer.Elapsed += Loging;
            transactionsList = new List<TransactionInfo>();

            _logTimer.Start();
            _withdrawTimer.Start();
        }

        public void AddVehicle(Vehicle vehicle)
        {            
            if (FindVehicleById(vehicle.Id) != null)
                throw new ArgumentException();
            else if (parking.vehicles.Count < Settings.ParkingCapacity)
                parking.vehicles.Add(vehicle);
            else
                throw new InvalidOperationException("Всі місця зайняті.");
        }

        public void Dispose()
        {
            _logTimer.Dispose();
            _withdrawTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity - parking.vehicles.Count;
        }

        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - parking.vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactionsList.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parking.vehicles.AsReadOnly();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var item = FindVehicleById(vehicleId);

            if (item == null)
                throw new ArgumentException();
            else if (item.Balance < 0)
                throw new InvalidOperationException("Транспорний засіб - боржник. Помилка видалення.");
            else
                parking.vehicles.Remove(item);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var item = FindVehicleById(vehicleId);

            if (item == null || sum <= 0)
                throw new ArgumentException();

            item.Balance += sum;            
        }

        private Vehicle FindVehicleById(string vehicleId)
        {
            var item = (from i in parking.vehicles
                        where i.Id == vehicleId
                        select i).ToArray();

            return item.Length > 0 ? item[0] : null;
        }

        public void Withdrawing(object sender, ElapsedEventArgs e)
        {
            foreach (var item in parking.vehicles)
            {
                decimal withdrawalAmount = Settings.Tariff[item.VehicleType];

                if (item.Balance < 0)
                    withdrawalAmount *= 2.5m;
                else if (item.Balance - withdrawalAmount < 0)
                    withdrawalAmount += (withdrawalAmount - item.Balance) * 2.5m;

                item.Balance -= withdrawalAmount;
                parking.Balance += withdrawalAmount;

                transactionsList.Add(new TransactionInfo(item.Id, withdrawalAmount));
            }
        }

        public void Loging(object sender, ElapsedEventArgs e)
        {
            _logService.Write(DateTime.Now.ToString() + "\tLogs: ");
            foreach (TransactionInfo item in transactionsList)
            {
                _logService.Write(item.ToString());
            }
            transactionsList.Clear();
        }
    }
}